from odoo import fields, models


class Member(models.Model):
    _inherit = 'res.partner'

    number = fields.Char(string='matricule : ')
    rent_ids = fields.One2many('rent.book', 'member_id')
