from odoo import fields, models, api
from odoo.exceptions import ValidationError


class Rent(models.Model):
    _name = 'rent.book'

    book_id = fields.Many2one('read.book', required=True)
    member_id = fields.Many2one('res.partner' ,required=True)
    state = fields.Selection([('1', 'Exemplaire prêté'), ('2', ' Exemplaire retourné'), ('3', ' Exemplaire perdu')],default='1')
    rent_date = fields.Date(string='Date Emprunt : ', default=fields.Datetime.now())
    return_date = fields.Date(string='Date Remis :')
