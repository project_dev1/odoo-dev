from odoo.tests.common import TransactionCase

from odoo.exceptions import ValidationError

import psycopg2

from datetime import timedelta, date


class TestBook(TransactionCase):

    def test__create(self):
        Read = self.env['read.book']

        book = Read.create({'name': 'Peter Pan', 'nbPage': 75})

        self.assertEqual(book.name, 'Peter Pan')
        self.assertEqual(book.nbPage, 75)
        self.assertEqual(book.date, False)
        self.assertEqual(book.like_count, 0)
        self.assertEqual(len(book.all_likes), 0)
        self.assertEqual(len(book.auth), 0)

    def test__check_unique_name(self):
        Read = self.env['read.book']

        book = Read.create({'name': 'Peter Pan', 'nbPage': 75})

        with self.assertRaises(psycopg2.IntegrityError):
            sameBook = Read.create({'name': 'Peter Pan', 'nbPage': 180})

    def test__zero_page(self):
        Read = self.env['read.book']

        with self.assertRaises(ValidationError):
            book = Read.create({'name': 'Peter Pan', 'nbPage': 0})

    def test__negative_pages(self):
        Read = self.env['read.book']

        with self.assertRaises(ValidationError):
            book = Read.create({'name': 'Peter Pan', 'nbPage': -1})

    def test__published_tomorrow(self):
        Read = self.env['read.book']

        with self.assertRaises(ValidationError):
            book = Read.create({'name': 'Peter Pan', 'nbPage': 150, 'date': (date.today() + timedelta(days=1))})
