from datetime import date

from odoo import fields, models, api
from odoo.exceptions import ValidationError


class Book(models.Model):
    _name = 'read.book'

    name = fields.Char(string='Titre', required=True)
    image = fields.Binary(string='Couverture')
    date = fields.Date(string='Date de publication')
    nbPage = fields.Integer(string='Nombre de pages', required=True)
    description = fields.Html(string='Description')
    like_count = fields.Integer(string='Book like count', compute="_compute_book_like_count")
    all_likes = fields.Many2many('res.users', string="likes of book")

    rent_count = fields.Integer(string='Book  rent', compute="_compute_book_rent_count")
    all_rent = fields.Many2many('res_partner', string="rent of book")

    auth = fields.Many2many('res.partner', string='Auteurs')

    _sql_constraints = [('book_name_unique', 'UNIQUE (name)', 'Book name must be unique!')]

    def _compute_book_like_count(self):
        for book in self:
            book.like_count = len(book.all_likes)

    def _compute_book_rent_count(self):
        for book in self:
            book.rent_count = len(book.all_rent)

    def like_book(self):
        if self.env.user in self.all_likes:
            self.all_likes -= self.env.user
        else:
            self.all_likes += self.env.user

    def rent_book(self):
        for rent in self:
            count = 0.0
            rents = rent.book_id.search([('book_id', '=', rent.id)])
            for member in rents:
                count += member.rent_ids
            rent.rent_count = count

    @api.constrains('date')
    def _check_date(self):
        for book in self:
            if book.date > date.today():
                raise ValidationError('Date must be earlier than today')

    @api.constrains('nbPage')
    def _check_nb_page(self):
        for book in self:
            if book.nbPage <= 0:
                raise ValidationError('Number of pages must be greater than 0')
