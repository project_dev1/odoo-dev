from odoo import models, fields, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    book_ids = fields.Many2many('read.book', string='Authors')
    isAuthor = fields.Boolean(default=False)
    author_book_count = fields.Integer("Author Book count", compute="_author_book_count", store=True)

    @api.depends('book_ids')
    def _author_book_count(self):
        for partner in self:
            if partner.isAuthor:
                partner.author_book_count = len(partner.book_ids)
