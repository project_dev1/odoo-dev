from odoo import models, fields, api


class ResPartner(models.Model):
    _inherit = 'product.template'

    list_books = fields.Many2many('read.book', string="Composés des livres", required=True)
