from odoo import models, fields, api
import logging
from odoo import exceptions

_logger = logging.getLogger(__name__)


class TodoTask(models.Model):
    _name = 'todo.task'

    name = fields.Char(string="nom de la tache", required=True)
    is_done = fields.Boolean('Done ?')
    active = fields.Boolean('Active ?', default=True)
    date_deadline = fields.Date('deadline')
    user_id = fields.Many2one('res.users', string='Responsible', default=lambda s: s.env.user)
    team_ids = fields.Many2many('res.partner', string='Team')

    def do_clear_done(self):
        if self.active:
            self.active = False
            _logger.info("Done do clear")
        else:
            raise exceptions.Warning("Task ins't active")

    def __str__(self):
        return f"Task(id={self.id}, name={self.name}, is_done={self.is_done}, active={self.active}, " \
               f"data_deadline={self.date_deadline}, user_name={self.user_id.name}, team_count={len(self.team_ids)})"

    def write(self, value):
        if not 'active' in value:
            value['active'] = True
            _logger.info(' Write', __name__)
        return super(TodoTask, self).write(value)

    def copy(self, default=None):
        default = dict(default or {})
        copied_count = self.search_count([('name', '=like', u"Copie de {}%".format(self.name))])

        if not copied_count:
            new_name = u"Copie de {}".format(self.name)
        else:
            new_name = u"Copie de {} ({})".format(self.name, copied_count)

        default['name'] = new_name
        return super(TodoTask, self).copy(default)
