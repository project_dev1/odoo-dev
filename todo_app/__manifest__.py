# -*- coding: utf-8 -*-
{
    'name': "todo_app",

    'summary': """
       new application of gestion of tasks""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Zidi Billal",
    'website': "http://www.yourcompany.com",
    'application': True,
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'security/security.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/res_partner_view.xml',
        'views/todo_view.xml',
        'views/todo_menu.xml',


    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
