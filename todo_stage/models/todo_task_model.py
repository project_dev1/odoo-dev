from odoo import fields, api, models
from odoo.exceptions import ValidationError


class TodoTAsk(models.Model):
    _name ='todo.task'
    _inherit = ['todo.task','mail.thread']

    effort_estimate = fields.Integer()
    tag_ids = fields.Many2many('todo.task.tag', string='Tags')
    desc = fields.Text('desc')
    state = fields.Selection([('draft', 'New'), ('open', 'Started'), ('done', 'Closed')], default='draft')
    docs = fields.Html('docs')
    date_created = fields.Datetime('date', default=fields.Datetime.now())
    image = fields.Binary('Image')
    user_todo_count = fields.Integer('task : ', compute='_compute_user_todo_count')

    def set_to_open(self):
        self.state = 'open'

    def set_to_closed(self):
        self.state = 'done'

    def set_to_draft(self):
        self.state = 'draft'

    def _compute_user_todo_count(self):
        for task in self:
            task.user_todo_count = task.search_count(
                [('user_id', '=', task.user_id.id)])

    @api.onchange('user_id')
    def _onchange_user_id(self):
        """ Reset teams """
        self.team_ids = None
        return {
            'warning': {
                'title': 'Responsible User Reset',
                'message': 'Please choose a new Team.',
            }
        }

    @api.constrains('name')
    def _check_name_size(self):
        for todo in self:
            if len(todo.name) < 5:
                raise ValidationError('Title must have 5 chars!')
